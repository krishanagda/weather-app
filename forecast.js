const key = "c72d7363c78235961dde9fc3f89adb5d";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base+query);
    if(response.ok){
        const data = await response.json();
        return data;
    }
    else{
        throw new Error("Status Code: "+response.status);
    }
}

getForecast("Mumbai")
.then(data => updateUI(data,"Mumbai"))
//.then(data=>console.log(data))
.catch(err => console.log(err));