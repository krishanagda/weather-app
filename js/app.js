(function($, document, window) {

    $(document).ready(function() {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function() {

    });

})(jQuery, document, window);

//Code for weather forecast starts from here
const cityForm = document.querySelector('form');
const defaultcity = "Mumbai";
const daysName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const monthsName = ["Jan", "Feb", "Mar","Apr", "May", "Jun","July","Aug","Sept","Oct", "Nov", "Dec"];
const updateUI = (data,cityName) => {
    $('.threehrtemp').remove();
    const days = document.getElementsByClassName('day');
    //console.log(days);
    const date = document.getElementsByClassName('date');
    const location = document.getElementsByClassName('location');
    const humidity = document.getElementById('humidity');
    const windSpeed = document.getElementById('wind-speed');
    const windDegree = document.getElementById('wind-degree');
    const temps = document.getElementsByClassName('temp');
    const icons = document.getElementsByClassName('weather-icon');
    console.log(data);
    location[0].innerHTML = cityName.charAt(0).toUpperCase() + cityName.slice(1);
    humidity.innerHTML = data.list[0].main.humidity + "%";
    windSpeed.innerHTML = Math.round(data.list[0].wind.speed * 3.6) + "Km/hr";
    windDegree.innerHTML = data.list[0].wind.deg + "<sup>o</sup>";
    
    const todaysDate = new Date(data.list[0].dt_txt);
    const todaysMonth = monthsName[todaysDate.getMonth()];
    const todaysDay = todaysDate.getDay();
    date[0].innerHTML = todaysDate.getDate() + " " + todaysMonth;
    
    var sameday = true;
    var j = 1; 
    while(sameday){
        const checkDate = new Date(data.list[j].dt_txt);
        if(checkDate.getDate() === todaysDate.getDate()){
            $('#todays_temp').append(`<div class="threehrtemp" id="element${j}">
                        <div class="time">${checkDate.getHours()} : 00</div>
                        <div class="num 3hrtemp"><span class="number">${Math.round((data.list[j].main.temp * 10)/10)}</span><sup>o</sup>C</div>
                    </div>`);
            j++;
        }
        else
            sameday = false;
    }
    
    var i = 0;
    for(let element of days){
        const dayName = daysName[(todaysDay + i)%7];
        element.innerHTML = dayName;
        
        let temp = Math.round((data.list[j].main.temp * 10)/10);
        console.log(data.list[j]);
        temps[i].innerHTML = temp + "<sup>o</sup>";
        icons[i].src = "images/icons/" + data.list[j].weather[0].icon + ".svg";
        i++;
        j+=8;
    }
}

cityForm.addEventListener('submit', e => {
    e.preventDefault();
    let cityName = cityForm.city.value.trim();
    if(cityName == ""){
        cityName = defaultcity;
    }
    
    getForecast(cityName)
    .then(data => updateUI(data,cityName))
//    .then(data => console.log(data))
    .catch(err => console.error(err));
});